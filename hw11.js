const eyeIcons = document.querySelectorAll(".icon-password");
const form = document.querySelector(".password-form");

eyeIcons.forEach((elem) => {
  elem.addEventListener("mousedown", () => {
    elem.classList.remove("fa-eye");
    elem.classList.add("fa-eye-slash");
    elem.previousElementSibling.type = "text";
  });
  elem.addEventListener("mouseup", () => {
    elem.classList.remove("fa-eye-slash");
    elem.classList.add("fa-eye");
    elem.previousElementSibling.type = "password";
  });
});

form.addEventListener("submit", (e) => {
  e.preventDefault();

  if (
    document.querySelector(".first-password").value ===
      document.querySelector(".second-password").value &&
    document.querySelector(".first-password").value !== "" &&
    document.querySelector(".second-password").value !== ""
  ) {
    alert("You are welcome");
    spanError.innerText = "";
  } else {
    document.querySelector(".error").innerText =
      "Потрібно ввести однакові значення";
  }
});
document
  .querySelector(".second-password").addEventListener("focus",
    () => (document.querySelector(".error").innerText = ""));
